/*
 * Filename: /mnt/d/Dev/Dreamcast/UB_SHARE/gamejam/game/examples/placeholder/src/player/player_info.h
 * Path: /mnt/d/Dev/Dreamcast/UB_SHARE/gamejam/game/examples/placeholder/src/player
 * Created Date: Thursday, January 1st 1970, 12:00:00 am
 * Author: HaydenKow
 * 
 * Copyright (c) 2020 Hayden Kowalchuk
 */

#pragma once
#include <common/common.h>
   
typedef struct player_info {
  uint32_t money;
  uint8_t happiness;
  uint8_t current_hour;
  uint8_t current_day;
  uint8_t current_week;
} player_info;

extern player_info player_stats;
extern player_info morning_stats;
extern bool day_over;
extern bool update_time;

bool PLYR_query(void);
void PLYR_save(void);
void PLYR_load(void);
void PLYR_init(void);
void PLYR_DrawUI(void);
void PLYR_DrawDayEnd(void);
void PLYR_ResetDay(void);
void PLYR_ResetWeek(void);
void PLYR_UpdateUI(void);
void PLYR_UpdateTime(float delta);
void PLYR_AddMoney(int change);
void PLYR_AddHappiness(int change);
