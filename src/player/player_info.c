/*
 * Filename: /mnt/d/Dev/Dreamcast/UB_SHARE/gamejam/game/examples/placeholder/src/player/player_info.c
 * Path: /mnt/d/Dev/Dreamcast/UB_SHARE/gamejam/game/examples/placeholder/src/player
 * Created Date: Thursday, January 1st 1970, 12:00:00 am
 * Author: HaydenKow
 * 
 * Copyright (c) 2020 Hayden Kowalchuk
 */

#include "player_info.h"

#include <common/input.h>
#include <common/renderer.h>
#include <stdbool.h>
#include <ui/ui_backend.h>

#include "../scenes/house_scene.h"

player_info player_stats;
player_info morning_stats;
static bool stats_loaded = false;

/* set to fullscreen on first boot */
static dimen_RECT pos = {.x = 0, .y = 0, .w = 640, .h = 54};

bool PLYR_query(void) {
  return stats_loaded;
}

void PLYR_save(void) {
/* Not supported yet*/
#ifdef _arch_dreamcast
  return;
#endif

  FILE *statfile;

  statfile = fopen("person.dat", "wb");
  if (statfile == NULL) {
    /* error couldnt open */
    return;
  }

  int written = fwrite(&player_stats, sizeof(player_info), 1, statfile);

  if (written != 0) {
    /* success */
  } else {
    /* fail */
  }

  fclose(statfile);
}

void PLYR_load(void) {
/* Not supported yet*/
#ifdef _arch_dreamcast
  return;
#endif
  FILE *statfile;

  statfile = fopen("person.dat", "rb");
  if (statfile == NULL) {
    /* error couldnt open */
    return;
  }

  fread(&player_stats, sizeof(player_info), 1, statfile);
  fclose(statfile);
}

void PLYR_init(void) {
  if (Sys_FileExists("person.dat")) {
    PLYR_load();
  } else {
    player_stats = (player_info){
        .money = 50, .happiness = 50, .current_hour = 6, .current_day = 0, .current_week = 0};
    PLYR_save();
  }

  memcpy(&morning_stats, &player_stats, sizeof(player_info));

  stats_loaded = true;
  pos.y = rect_pos_fullscreen.h - pos.h;
  pos.w = rect_pos_fullscreen.w;
  PLYR_UpdateUI();
}

void PLYR_AddMoney(int change) {
  player_stats.money += change;
  PLYR_UpdateUI();
}

void PLYR_AddHappiness(int change) {
  player_stats.happiness += change;
  PLYR_UpdateUI();
}

char msg_time_date[32];
char msg_money_cash[8];
char msg_happiness_percent[32];
const char *msg_money = "Money; ";
const char *msg_happiness = "Happiness; ";
char msg_rent[32];
const unsigned int RENT = 300;

char msg_money_end[8];
char msg_happy_end[8];

/* 10 minutes to 17hrs */
/* 600 seconds to 17hrs */
/* every 35.294s is an hour */
//#define TIME_SCALE (35.294f)
#define TIME_SCALE (5.0f)
static float realtime_counter = 0;
bool day_over = false;
bool update_time = true;

void PLYR_UpdateTime(float delta) {
  if ((!day_over) && update_time) {
    realtime_counter += delta;
    if (realtime_counter > TIME_SCALE) {
      player_stats.current_hour++;
      realtime_counter -= TIME_SCALE;
      if (player_stats.current_hour > 24) {
        day_over = true;
        update_time = false;
        return;
      }
      PLYR_UpdateUI();
    }
  }
}

void PLYR_ResetDay(void) {
  realtime_counter = 0;
  player_stats.current_hour = 6;
  player_stats.current_day++;
  if (player_stats.current_day > 6) {
    player_stats.current_week++;
    player_stats.current_day = 0;
    PLYR_ResetWeek();
  }
  day_over = false;
  memcpy(&morning_stats, &player_stats, sizeof(player_info));
}

void PLYR_ResetWeek(void) {
  realtime_counter = 0;
  /* stuff? */
}

void PLYR_UpdateUI(void) {
  sprintf(msg_time_date, "06/%02d/2009 %02d:00 %s", player_stats.current_day + 1 + (player_stats.current_week * 7), player_stats.current_hour, (player_stats.current_hour > 11) ? "pm" : "am");
  sprintf(msg_money_cash, "$%d", player_stats.money);
  sprintf(msg_happiness_percent, "%d", player_stats.happiness); /* percent symbol is broken ?*/
}

void PLYR_DrawUI(void) {
  if (day_over) {
    PLYR_DrawDayEnd();
  } else {
    /* Bottom Quad: Draw blue*/
    UI_DrawFill(&pos, 40, 112, 128);

    UI_TextColor(1, 1, 1);
    UI_TextSize(22);

    /* date/time */
    UI_DrawString(4, rect_pos_fullscreen.h - (26 * 2), msg_time_date);

    /* money */
    UI_DrawString(4, rect_pos_fullscreen.h - 24, msg_money);
    UI_TextColor(0, 1, 0);
    UI_DrawString(4 + (text_size * 6), rect_pos_fullscreen.h - 24, msg_money_cash);

    /* happiness */
    UI_TextColor(1, 1, 1);
    UI_DrawString(220, rect_pos_fullscreen.h - 24, msg_happiness);
    UI_TextColor(MIN(2.0f * (player_stats.happiness / 100.0f), 1.0f), MIN(2.0f * (1.0f - (player_stats.happiness / 100.0f)), 1.0f), 0);
    UI_DrawString(220 + (26 * 9), rect_pos_fullscreen.h - 24, msg_happiness_percent);
  }
}

void PLYR_DrawDayEnd(void) {
  UI_DrawFadeScreen();

  /* Draw Box */
  int height = 200;
  int width = (int)(height * 3.0f);
  int thickness = 4;

  dimen_RECT quad = {.x = 640 / 2 - width / 2, .y = 480 / 2 - height / 2, .w = width, .h = height};
  UI_DrawFill(&quad, 0.0f, 0.0f, 0.0f);

  quad.w = width;
  quad.h = thickness;
  UI_DrawFill(&quad, 0, 255.0f, 255.0f);  //Top
  quad.w = thickness;
  quad.h = height;
  UI_DrawFill(&quad, 0, 255.0f, 255.0f);  //Left
  quad.y = (480 / 2 + height / 2) - thickness;
  quad.w = width;
  quad.h = thickness;

  UI_DrawFill(&quad, 0, 255.0f, 255.0f);  //Bottom

  quad.x = 640 / 2 + width / 2;
  quad.y = 480 / 2 - height / 2;
  quad.w = thickness;
  quad.h = height;
  UI_DrawFill(&quad, 0, 255.0f, 255.0f);  //Right

  /* Now Text */
  UI_TextColor(1, 1, 1);
  UI_TextSize(32);
  UI_DrawStringCentered(640 / 2, 480 / 2 - (UI_TextSizeGet() * 2), "End of the Day");

  UI_TextSize(22);
  UI_DrawStringCentered(320 - 64, (480 / 2) - 32, " Money:");
  UI_DrawStringCentered(320 - 64, (480 / 2) - 0, " Happiness:");

  //UI_DrawStringCentered(640 / 2, ((480 / 2 - 200 / 2) + 200) - text_size, msg_rent);
  UI_DrawStringCentered(640 / 2, (480 / 2) + 30, msg_rent);

  const int money_diff = (int)player_stats.money - (int)morning_stats.money;
  const int happiness_diff = (int)player_stats.happiness - (int)morning_stats.happiness;

  sprintf(msg_rent, "$%d Rent is %d days away", RENT, 6 - player_stats.current_day);
  sprintf(msg_money_end, "%d", money_diff);
  sprintf(msg_happy_end, "%d", happiness_diff);

  if (money_diff > 0) {
    UI_TextColor(0, 1, 0);
  } else if (money_diff < 0) {
    UI_TextColor(1, 0, 0);
  } else {
    UI_TextColor(1, 1, 1);
  }
  UI_DrawStringCentered(320 - 64 + (10 * text_size), (480 / 2) - 32, msg_money_end);
  if (happiness_diff > 0) {
    UI_TextColor(0, 1, 0);
  } else if (happiness_diff < 0) {
    UI_TextColor(1, 0, 0);
  } else {
    UI_TextColor(1, 1, 1);
  }
  UI_DrawStringCentered(320 - 64 + (10 * text_size), (480 / 2) - 0, msg_happy_end);

  UI_TextSize(20);
  UI_DrawStringCentered((640 / 2) - 140, 320, "Continue");
  UI_DrawStringCentered((640 / 2) + 140, 320, "Save & Exit");

  UI_TextSize(32);
  UI_TextColor(1, 1, 1);
  UI_DrawStringCentered((640 / 2) - 140 - 94, 320, STRING_A_BTN);
  UI_DrawStringCentered((640 / 2) + 140 + 128, 320, STRING_Y_BTN);

  if (INPT_ButtonEx(BTN_START, BTN_RELEASE) || INPT_ButtonEx(BTN_A, BTN_RELEASE)) {
    /* Save and Continue */
    PLYR_ResetDay();
    PLYR_save();
    is_entering_house = false;
    update_time = true;
    SCN_ChangeTo(game_house);
  }
  if (INPT_Button(BTN_Y)) {
    /* Save and Exit */
    PLYR_ResetDay();
    PLYR_save();
    Sys_Quit();
  }
}
