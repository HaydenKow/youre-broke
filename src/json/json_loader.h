
#pragma once

#include <common/common.h>
#include <common/obj_loader.h>
#include <common/renderer.h>

typedef struct json_object {
  char name[24];
  char id[24];
  vec3f pos;
  vec3f rot;
  vec3f scale;
  float orig_scale;
  int type;
  model_obj *mesh;
  tx_image *texture;
} json_object;

void JSON_SetAssetDir(const char *dir);
int JSON_ParseDropper(const char *filename, json_object *objs, size_t max_objs, json_object *triggers, size_t max_triggers, json_object *camera, int *read_objects, int *read_triggers);
