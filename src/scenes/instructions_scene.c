/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\placeholder\src\scenes\instructions_scene.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\placeholder\src\scenes
 * Created Date: Monday, May 4th 2020, 2:46:51 pm
 * Author: HaydenKow
 * 
 * Copyright (c) 2020 Hayden Kowalchuk
 */

#include "instructions_scene.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/renderer.h>
#include <ui/ui_backend.h>

#include "scenes.h"

static void Instructions_Init(void);
static void Instructions_Exit(void);
static void Instructions_Update(float time);
static void Instructions_Render2D(float time);

/* Registers the Title scene and populates the struct */
//STARTUP_SCENE(&Instructions_Init, &Instructions_Exit, &Instructions_Render2D, NULL, &Instructions_Update, SCENE_BLOCK);
SCENE(scene_instructions, &Instructions_Init, &Instructions_Exit, &Instructions_Render2D, NULL, &Instructions_Update, SCENE_BLOCK);

static uint8_t current_page;
static float time_menu;

static void Instructions_Init(void) {
  current_page = 0;
  time_menu = 0;
}

static void Instructions_Exit(void) {
}

static void Instructions_Update(float time) {
  (void)time;
  float now = Sys_FloatTime();

  /* Handle input every so often */
  if (now - time_menu > 0.12f) {
    extern void SND_Land(void);

    dpad_t dpad = INPT_DPAD() & 15;
    switch (dpad) {
      case DPAD_LEFT:
        if (current_page != 0)
          current_page--;
        SND_Land();
        time_menu = now;
        break;
      case DPAD_RIGHT:
        if (current_page != 2)
          current_page++;

        SND_Land();
        time_menu = now;
        break;
    }
  }

  if (INPT_ButtonEx(BTN_B, BTN_RELEASE)) {
    SCN_ChangeTo(main_title);
  }
}

static void instruction_page1(void) {
  /* Title */
  UI_DrawStringCentered(640 / 2, 12, "Gameplay");

  /* Text */
  UI_TextSize(24);
  UI_DrawString(12, 40, "You're trying to survive");
  UI_DrawString(12, 40+(text_size+8)*1, "the tough world.");
  UI_DrawString(12, 40+(text_size+8)*2, "Make Money to do this!");
}

static void instruction_page2(void) {
  /* Title */
  UI_DrawStringCentered(640 / 2, 12, "Controls");

  /* Text */
  UI_TextSize(24);
  UI_DrawString(12, 40, "Analog Controls you!");
  UI_DrawString(12, 40+(text_size+8)*1, "DPAD and " STRING_A_BTN " for menus");
}

static void instruction_page3(void) {
  /* Title */
  UI_DrawStringCentered(640 / 2, 12, "Tips");

  /* Text */
  UI_TextSize(24);
  UI_DrawString(12, 40, "Try to stay happy");
  UI_DrawString(12, 40+(text_size+8)*1, "Pay rent each week");
  UI_DrawString(12, 40+(text_size+8)*2, "Get a job");
  UI_DrawString(12, 40+(text_size+8)*3, "Learn to cook");
}

static void Instructions_Render2D(float time) {
  (void)time;
  UI_TextSize(28);

  /* Background Black */
  UI_DrawFill(&rect_pos_fullscreen, 0, 0, 0);

  if (time_waiting > 1.0f)
    UI_TextColorEx(1.0f, 1.0f, 1.0f, (1.0f - ((time_waiting - 1) / 2.0f)));

  /* Holds all pages as a jump table */
  static void (*draw_page[])(void) = {instruction_page1, instruction_page2, instruction_page3};

  if (current_page < sizeof(draw_page) / sizeof(*draw_page)) {
    draw_page[current_page]();
  }

  /* Page markers */
  char pageNum = '1' + current_page;
  char pageNumStr[2];
  pageNumStr[0] = pageNum;
  pageNumStr[1] = '\0';
  char pageStr[8] = {0};
  strcat(pageStr, "Page ");
  strcat(pageStr, pageNumStr);

  UI_DrawStringCentered(rect_pos_fullscreen.w / 2, rect_pos_fullscreen.h - text_size - 8, pageStr);

  pageNumStr[0] = 13; /* Arrow */

  /* draw right facing arrow everywhere except last */
  if (current_page != (sizeof(draw_page) / sizeof(*draw_page)) - 1) {
    UI_DrawStringCentered(rect_pos_fullscreen.w - text_size - 8, rect_pos_fullscreen.h - text_size - 8, pageNumStr);
  }
  /* draw left facing arrow everywhere except first */
  if (current_page != 0) {
    UI_DrawStringCentered_Rot(8, rect_pos_fullscreen.h - text_size - 8, pageNumStr, M_PI);
  }
}
