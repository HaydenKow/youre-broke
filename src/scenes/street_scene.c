/*
 * File: street_scene.c
 * Project: scenes
 * File Created: Thursday, 7th May 2020 1:26:20 am
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#include "street_scene.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/jfm_loader.h>
#include <common/math_headers.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <ui/ui_backend.h>

#include "../json/json_loader.h"
#include "../player/player_info.h"
#include "dialog_popup.h"
#include "downtown_scene.h"
#include "house_scene.h"

/* Dialog */
#include "../trigger/car.h"
#include "../trigger/house.h"
#include "../trigger/neighbor.h"
#include "../trigger/response.h"
#include "../trigger/street.h"

#define PLAYER_SCALE (0.064f)

extern GLfloat _box_vertices3[144];
extern GLfloat _box_vertices_blender3[144];
extern GLubyte _box_indices[36];

static void StreetScene_Init(void);
static void StreetScene_Exit(void);
static void StreetScene_Update(float time);
static void StreetScene_Render2D(float time);
static void StreetScene_Render3D(float time);

/* Registers the Title scene and populates the struct */
//STARTUP_SCENE(&StreetScene_Init, &StreetScene_Exit, &StreetScene_Render2D, &StreetScene_Render3D, &StreetScene_Update, SCENE_BLOCK);
SCENE(game_street, &StreetScene_Init, &StreetScene_Exit, &StreetScene_Render2D, &StreetScene_Render3D, &StreetScene_Update, SCENE_BLOCK);

static void Draw_Objects(void);
static void __attribute__((unused)) draw_cube(void);
static void draw_player(void);

static json_object objs[32];
static json_object triggers[32];
static json_object camera;
static int read_objects;
static int read_triggers;

static int last_hit_trigger;

/*
static model_jfm *dreamcast_jfm;
static tx_image *dreamcast_tex;
*/

static json_object player;

static tx_image *shadow_tex;
static json_object cube;

static vec3f camera_rot_base;
static int objects = 0;

static tx_image *ui_texture;
static sprite circle, circle_trans;

static float level_adjust_angle;  // = (-M_PI / 2);  //-90.0f;

void StreetScene_MoveToHouse(int choice);
void StreetScene_MoveToDowntown(int choice);

static void StreetScene_Init(void) {
/*unused here */
#if 0
  dreamcast_jfm = JFM_load("assets/model/dreamcast.jfm");
  dreamcast_tex = IMG_load("assets/model/dreamcast.png");
  RNDR_CreateTextureFromImage(dreamcast_tex);
  dreamcast_jfm->texture = dreamcast_tex->id;
#endif

  player.mesh = OBJ_load("assets/basicCharacter.obj");
  player.texture = IMG_load("assets/skin_man.png");
  RNDR_CreateTextureFromImage(player.texture);
  player.mesh->texture = player.texture->id;
  strcpy(player.name, "player");
  player.pos = (vec3f){0, 0, 0};
  player.rot = (vec3f){0, 0, 0};
  player.scale = (vec3f){1, 1, 1};

  memset(objs, '\0', sizeof(objs));
  JSON_SetAssetDir("assets/street/");
  if (!(objects = JSON_ParseDropper("assets/levels/street.txt", objs, 32, triggers, 32, &camera, &read_objects, &read_triggers))) {
    printf("ERROR!\n");
  }

  camera_rot_base = camera.rot;
  shadow_tex = IMG_load("assets/shadow_square.png");
  RNDR_CreateTextureFromImage(shadow_tex);

  for (int i = 0; i < read_triggers; i++) {
    if (!stricmp(triggers[i].id, "load")) {
      player.pos = (vec3f){triggers[i].pos.x * (1 / PLAYER_SCALE), 0, triggers[i].pos.z * (1 / PLAYER_SCALE)};
      last_hit_trigger = i;
      break;
    }
  }

  if (!PLYR_query())
    PLYR_init();

  /* Load Assets */
  ui_texture = IMG_load("assets/ui_stuff.png");
  RNDR_CreateTextureFromImage(ui_texture);
  circle = IMG_create_sprite_alt(ui_texture, 2, 2, 32, 32);
  circle_trans = IMG_create_sprite_alt(ui_texture, 36, 2, 32, 32);

  level_adjust_angle = (M_PI / 2);  //90.0f;

  DIALOG_Reset();
}

static void StreetScene_Exit(void) {
}

static void StreetScene_Update(float time) {
  if (day_over) {
    return;
  }
  if (DIALOG_IsVisible()) {
    return;
  }

  PLYR_UpdateTime(time);
#ifdef PSP
  static float speed = 1.0f;
#else
  static float speed = 0.5f;
#endif
  static float angle = 0.0f;

  {
    float rad = 0.0f;
    float _x = INPT_AnalogF(AXES_X);
    float _y = INPT_AnalogF(AXES_Y);

    float x = _x * SQRT(1 - _y * _y * 0.5f);
    float y = _y * SQRT(1 - _x * _x * 0.5f);

    /*
    if (y >= 0) {
      if (x == 0) {
        rad = M_PI / 2;
      } else {
        rad = atanf(y / x);
      }
    } else if (y < 0) {
      if (x == 0) {
        rad = M_PI / 2 + M_PI;
      } else {
        rad = atanf(y / x) + M_PI;
      }
    }
    */
    rad = atan2f(y, x);
    //angle = level_adjust_angle - (rad * 360 / (M_PI * 2));
    angle = level_adjust_angle - rad;
    player.rot.z = angle;
#define DEADZONE_FORWARD (0.30f)
    if ((fabs(x) > DEADZONE_FORWARD) || (fabs(y) > DEADZONE_FORWARD)) {
      player.pos.x += (sinf(player.rot.z) * speed);
      player.pos.z += (cosf(player.rot.z) * speed);
    }

/* position */
#if defined(__MINGW32__) || defined(__linux__)
    if (INPT_DPADDirection(DPAD_LEFT)) {
      level_adjust_angle += 0.05f;
    }
    if (INPT_DPADDirection(DPAD_RIGHT)) {
      level_adjust_angle -= 0.05f;
    }
    if (INPT_DPADDirection(DPAD_UP)) {
      player.pos.x += (sinf(player.rot.z) * speed);
      player.pos.z += (cosf(player.rot.z) * speed);
    }
#endif
  }
}

static void StreetScene_Render2D(float time) {
  (void)time;
  UI_TextSize(14);
  UI_TextColorEx(1, 1, 1, 1);

  /* Player position */
  {
#if 0
    char msg[64];
    //snprintf(msg, 64, "player {%5.2f, %5.2f}", player.pos.x*0.065f, player.pos.z*0.065f);
    snprintf(msg, 64, "player {%5.2f, %5.2f}", player.pos.x, player.pos.z);
    UI_DrawStringCentered(640 / 2, 16, msg);
#endif
  }

  /* Trigger position */
  {
    int i = 0;
    bool hit_trigger = false;
    while (triggers[i].name[0] != '\0') {
      if ((player.pos.x > ((triggers[i].pos.x - triggers[i].scale.x) * (1 / PLAYER_SCALE)) && player.pos.x < ((triggers[i].pos.x + triggers[i].scale.x) * (1 / PLAYER_SCALE))) /* x*/
          && (player.pos.z > ((triggers[i].pos.z - triggers[i].scale.y) * (1 / PLAYER_SCALE)) && player.pos.z < ((triggers[i].pos.z + triggers[i].scale.y) * (1 / PLAYER_SCALE)))) /* y */ {
        /* Trigger Debugging */
        {
#if 0
          strcpy(msg, "Trigger: ");
          strcat(msg, triggers[i].id);
          UI_DrawStringCentered(640 / 2, 440, msg);

          snprintf(msg, 64, "trigger {%5.2f, %5.2f } {%5.2f, %5.2f}", triggers[i].pos.x * (1 / PLAYER_SCALE), triggers[i].pos.z * (1 / PLAYER_SCALE), triggers[i].scale.x * (1 / PLAYER_SCALE), triggers[i].scale.y * (1 / PLAYER_SCALE));
          UI_DrawStringCentered(640 / 2, 32, msg);
#endif
        }
        if (!stricmp(triggers[i].id, "load")) {
          i++;
          continue;
        }
        hit_trigger = true;
        if (i == last_hit_trigger) {
          break;
        }
        printf("TRigger: hit! %s\n", triggers[i].id);
        if (!stricmp(triggers[i].id, "house")) {
          /* Dialog Popup */
          DIALOG_SetType(Choice);
          DIALOG_SetDefaultTitle();
          DIALOG_SetDefaultOptions();
          DIALOG_SetCallback(&StreetScene_MoveToHouse);
          char temp[64];
          strncpy(temp, house_enter->responses[rand() % house_enter->num], 63);
          DIALOG_SetText(temp);
          DIALOG_SetVisible(true);
        }
        if (!stricmp(triggers[i].id, "downtown")) {
          /* Dialog Popup */
          DIALOG_SetType(Choice);
          DIALOG_SetDefaultTitle();
          DIALOG_SetDefaultOptions();
          DIALOG_SetCallback(&StreetScene_MoveToDowntown);
          char temp[64];
          strncpy(temp, downtown_enter->responses[rand() % downtown_enter->num], 63);
          DIALOG_SetText(temp);
          DIALOG_SetVisible(true);
        }
        if (!stricmp(triggers[i].id, "neighbor")) {
          /* Dialog Notice */
          DIALOG_SetType(Notice);
          DIALOG_SetTitle("Notice");
          DIALOG_SetOptions("Okay", "");
          DIALOG_SetCallback(NULL);
          char temp[64];
          strncpy(temp, neighbor_away->responses[rand() % neighbor_away->num], 63);
          DIALOG_SetText(temp);
          DIALOG_SetVisible(true);
        }

        if (!stricmp(triggers[i].id, "car")) {
          /* Dialog Notice */
          DIALOG_SetType(Notice);
          DIALOG_SetTitle("Notice");
          DIALOG_SetOptions("Okay", "");
          DIALOG_SetCallback(NULL);
          char temp[64];
          strncpy(temp, car_unowned->responses[rand() % car_unowned->num], 63);
          DIALOG_SetText(temp);
          DIALOG_SetVisible(true);
        }
        last_hit_trigger = i;
        break;
      }
      i++;
    }
    /* if not touching anything reset */
    if (!hit_trigger) {
      last_hit_trigger = -1;
    }
  }

  PLYR_DrawUI();
  DIALOG_Draw(time);
}

void StreetScene_MoveToHouse(int choice) {
  if (choice) {
    is_entering_house = true;
    SCN_ChangeTo(game_house);
  }
}

void StreetScene_MoveToDowntown(int choice) {
  if (choice) {
    is_entering_house = false;
    SCN_ChangeTo(game_downtown);
  }
}

static void StreetScene_Render3D(float time) {
  static vec3f camera_front;
  static vec3f camera_look;
  glPushMatrix();
  glLoadIdentity();

  static float counter = 0.0f;
  counter += (time * 60);

  vec3f direction;
  direction.x = COS(camera.rot.z) * COS(camera.rot.x);
  direction.y = SIN(camera.rot.x);
  direction.z = SIN(camera.rot.z) * COS(camera.rot.x);

  // x: pitch y: roll z: yaw
  glm_vec3_normalize((float *)&direction.x);
  camera_front = direction;

  glm_vec3_sub((float *)&camera.pos.x, (float *)&camera_front.x, (float *)&camera_look.x);

  mat4 view;
  /* Old */
  //vec3f cameraUp = (vec3f){0.0f, 1.0f, 0.0f};
  //gluLookAt(camera.pos.x, camera.pos.y, camera.pos.z, player.pos.x * PLAYER_SCALE, 0, player.pos.z * PLAYER_SCALE, cameraUp.x, cameraUp.y, cameraUp.z);
  glm_lookat((float *)&camera.pos, (vec3){player.pos.x * PLAYER_SCALE, 0, player.pos.z * PLAYER_SCALE}, GLM_YUP, view);
  glLoadMatrixf((float *)view);

  Draw_Objects();

  draw_player();
  //draw_cube();
  //draw_shadow();
  //glPushMatrix();
  /*
  glTranslatef(0.0f,-2.0f,-10.0f);
  // Rotation based on user input
  glRotatef(-0, 0.0f, 1.0f, 0.0f);
  glRotatef(-0, 1.0f, 0.0f, 0.0f);
  glRotatef(110, 1.0f, 0.0f, 0.0f);
  */
  //draw_jfm(dreamcast_jfm);
  //glPopMatrix();
  glPopMatrix();
}

static void draw_cube(void) {
  /* Setup VA params */
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  glDisable(GL_TEXTURE_2D);
  glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3 + 3);
  glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3);

  /*Update Object Matrix */
  glPushMatrix();
  glTranslatef(cube.pos.x, cube.pos.y, cube.pos.z);
  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
  glPopMatrix();
}

static void draw_player(void) {
  glPushMatrix();
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
  if (player.texture != 0)
    glBindTexture(GL_TEXTURE_2D, player.texture->id);

  glEnable(GL_TEXTURE_2D);
  OBJ_bind(player.mesh);

  //glTranslatef(0, 0, 0);
  //glRotatef(counter, 0, 1, 0);

  float normalizeAmount = PLAYER_SCALE;
  glScalef(normalizeAmount, normalizeAmount, normalizeAmount);
  glTranslatef(player.pos.x, player.pos.y, player.pos.z);
  glRotatef(RAD2DEG(player.rot.z), 0, 1, 0);
  glDrawArrays(GL_TRIANGLES, 0, player.mesh->num_tris);
  glPopMatrix();
}

static void Draw_Objects(void) {
  if (objects <= 0) {
    return;
  }
  for (int i = 0; i < objects; i++) {
    if (objs[i].orig_scale == 0.0f) {
      objs[i].orig_scale = 1.0f;
    }
    /* Mesh */
    if (objs[i].mesh != NULL) {
      glEnableClientState(GL_VERTEX_ARRAY);
      glEnableClientState(GL_COLOR_ARRAY);
      glEnableClientState(GL_TEXTURE_COORD_ARRAY);
      glDisable(GL_TEXTURE_2D);

      if (objs[i].texture != NULL) {
        GL_Bind(objs[i].texture);
        glEnable(GL_TEXTURE_2D);
      }
      OBJ_bind(objs[i].mesh);

      /*Update Object Matrix */
      glPushMatrix();
      glTranslatef(objs[i].pos.x, objs[i].pos.y, objs[i].pos.z);

      /*if (i == 7) {
        glScalef(0.1f, 0.1f, 0.1f);
        glRotatef(90, 1.0f, 0.0f, 0.0f);
        draw_jfm(dreamcast_jfm);
      } else*/
      {
        glRotatef(RAD2DEG(objs[i].rot.x) - 90.0f, 1, 0, 0);
        //glRotatef(RAD2DEG(objs[i].rot.y), 0, 0, 1);
        glRotatef(RAD2DEG(objs[i].rot.z), 0, 1, 0);
        glScalef(objs[i].scale.x / objs[i].orig_scale, objs[i].scale.y / objs[i].orig_scale, objs[i].scale.z / objs[i].orig_scale);
        glDrawArrays(GL_TRIANGLES, 0, objs[i].mesh->num_tris);
      }
      glPopMatrix();

    } else {
#if 0
      /* Cube */

      /* Setup VA params */
      glEnableClientState(GL_VERTEX_ARRAY);
      glEnableClientState(GL_COLOR_ARRAY);

      glDisableClientState(GL_TEXTURE_COORD_ARRAY);
      if (objs[i].texture != NULL) {
        glEnable(GL_TEXTURE_2D);
        GL_Bind(objs[i].texture);
      } else {
        glDisable(GL_TEXTURE_2D);
      }

      glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3 + 3);
      glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3);

      /*Update Object Matrix */
      glPushMatrix();
      glTranslatef(objs[i].pos.x, objs[i].pos.y, objs[i].pos.z);
      //glRotatef(RAD2DEG(objs[i].rot.x), 1, 0, 0);
      //glRotatef(RAD2DEG(objs[i].rot.y), 0, 0, 1);
      //glRotatef(RAD2DEG(objs[i].rot.z), 0, 1, 0);
      glScalef(objs[i].scale.x, objs[i].scale.z, objs[i].scale.y);
      glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
      glPopMatrix();
#endif
    }
  }
}
