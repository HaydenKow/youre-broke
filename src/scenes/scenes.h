/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\placeholder\scenes\scenes.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\placeholder\scenes
 * Created Date: Monday, May 4th 2020, 2:00:56 pm
 * Author: HaydenKow
 * 
 * Copyright (c) 2020 Hayden Kowalchuk
 */

#pragma once

#include "intro_scene.h"
#include "main_title.h"
#include "instructions_scene.h"
#include "house_scene.h"