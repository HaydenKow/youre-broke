/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\placeholder\scenes\intro_scene.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\placeholder\scenes
 * Created Date: Monday, May 4th 2020, 1:52:10 pm
 * Author: HaydenKow
 * 
 * Copyright (c) 2020 Hayden Kowalchuk
 */

#include "intro_scene.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/renderer.h>
#include <common/resource_manager.h>
#include <ui/ui_backend.h>

#include "scenes.h"

WINDOW_TITLE("Placeholder", WINDOWED);

static void Intro_Init(void);
static void Intro_Exit(void);
static void Intro_Update(float time);
static void Intro_Render2D(float time);

/* Registers the Intro scene and populates the struct */
STARTUP_SCENE(&Intro_Init, &Intro_Exit, &Intro_Render2D, NULL, &Intro_Update, SCENE_BLOCK);
SCENE(scene_intro, &Intro_Init, &Intro_Exit, &Intro_Render2D, NULL, &Intro_Update, SCENE_BLOCK);

static tx_image *jam_neodc_image, *logo2;

static sprite logo_neodc, logo_psp;

static void Intro_Init(void) {
  /* 1: psp jam */
  /* 2: neodc */
  /* 3: integrity logo */
  jam_neodc_image = IMG_load("assets/images/logos.png");
  RNDR_CreateTextureFromImage(jam_neodc_image);
  logo_neodc = IMG_create_sprite_alt(jam_neodc_image, 1, 62, 192, 192);
  logo_psp = IMG_create_sprite_alt(jam_neodc_image, 0, 0, 256, 60);

  logo2 = IMG_load("assets/images/Integrity_S_trans_white_SQ.png");
  RNDR_CreateTextureFromImage(logo2);
  time_waiting = 0.0f;
  current_intro = 1;
}

static void Intro_Exit(void) {
  resource_object_remove(jam_neodc_image->crc);
  resource_object_remove(logo2->crc);
}

static void Intro_Update(float time) {
  time_waiting += time;
  if (time_waiting >= 3.0f) {
    time_waiting = 0.0f;
    current_intro++;
  }
  if (current_intro == 4) {
    SCN_ChangeTo(main_title);
  }
}

static void Intro_Render2D(float time) {
  (void)time;
  UI_TextSize(24);

  /* Background Teal */
  UI_DrawFill(&rect_pos_fullscreen, 0, 0, 0);

  if (time_waiting > 1.0f)
    UI_TextColorEx(1.0f, 1.0f, 1.0f, (1.0f - ((time_waiting - 1) / 2.0f)));

  switch (current_intro) {
    case 1: {
      dimen_RECT pos = {10, 240 - (145 / 2), 620, 145}; /* full width, scaled height */
      UI_DrawTransSprite(&pos, text_alpha, &logo_psp);
      UI_DrawStringCentered(640 / 2, 400, "Entry Submission");
    } break;
    case 2: {
      //UI_DrawTransPicCentered(640 / 2, 480 / 2, logo1);
      dimen_RECT pos = {(640 / 2) - (256 / 2), (480 - 256) / 2, 256, 256};
      UI_DrawTransSprite(&pos, text_alpha, &logo_neodc);
      UI_DrawStringCentered(640 / 2, 400, "created by");
      UI_DrawStringCentered(640 / 2, 430, "mrneo240");
    } break;
    case 3: {
      UI_DrawTransPicCentered(640 / 2, 480 / 2, logo2);
    }

    break;
  }
}
