/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\level_select.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Sunday, November 17th 2019, 10:11:21 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "main_title.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/renderer.h>
#include <ui/ui_backend.h>

#include "scenes.h"

static unsigned char selected_index;
static float time_menu;
static float time_show;

static bool _fade_in = false;
static const char *headers[4] = {"Continue", "New", "Instructions", "Quit"};

static void Title_Init(void);
static void Title_Destroy(void);
static void Title_Update(float time);
static void Title_Render2D(float time);

/* Registers the Title scene and populates the struct */
//STARTUP_SCENE(&Title_Init, NULL, &Title_Render2D, NULL, &Title_Update, SCENE_BLOCK);
SCENE(main_title, &Title_Init, &Title_Destroy, &Title_Render2D, NULL, &Title_Update, SCENE_BLOCK);

#if defined(_arch_dreamcast) || defined(PSP)
#define MENU_ITEMS 3
#else
#define MENU_ITEMS 4
#endif
/* Scene Assets */
static tx_image *ui_texture;
static sprite title_logo;

/* Sets our default values on the Title scene */
static void Title_Init(void) {
  selected_index = 0;
  time_menu = 0;
  time_show = 0;

  /* Load Assets */
  ui_texture = IMG_load("assets/ui_stuff.png");
  RNDR_CreateTextureFromImage(ui_texture);
  title_logo = IMG_create_sprite_alt(ui_texture, 0, 90, 256, 168);
}

static void Title_Destroy(void) {
}

/* Check input and move selection Indicator */
static void Title_Update(float time) {
  float now = Sys_FloatTime();

  /* Handle input every so often */
  if (now - time_menu > 0.12f) {
    extern void SND_Land(void);

    dpad_t dpad = INPT_DPAD() & 15;
    switch (dpad) {
      case DPAD_UP:
        if (selected_index != 0)
          selected_index--;
        SND_Land();
        time_menu = now;
        break;
      case DPAD_DOWN:
        if (selected_index != (MENU_ITEMS - 1))
          selected_index++;

        SND_Land();
        time_menu = now;
        break;
    }
  }

  /* Execute Action */
  if (INPT_ButtonEx(BTN_A, BTN_PRESS)) {
    switch (selected_index) {
      case 0:
        SCN_ChangeTo(game_house);
        break;
      case 1:
        //SCN_ChangeTo(scene_credits);
        break;
      case 2:
        SCN_ChangeTo(scene_instructions);
        break;
      case 3:
        Sys_Quit();
        break;
    }
  }

  time_show -= time;
}

/* Draws the 2D menu, time param unused */
static void Title_Render2D(float time) {
  (void)time;
  static float _next = 0;
  float now = Sys_FloatTime();

  if (now > _next) {
    _next = now + 0.5f;
    _fade_in = !_fade_in;
  }

  /* Background Black */
  UI_DrawFill(&rect_pos_fullscreen, 0, 0, 0);

  dimen_RECT pos = {.x = 100, .y = 48, .h = 324, .w = 460};
  UI_DrawTransSprite(&pos, 1.0f, &title_logo);

  /* Menu Options */
  const int spacing_h = 4;

  /* Dont show Quit on console, start lower on screen */
  const int start_x = 8;
#if defined(__arch_dreamcast) || defined(PSP)
  const int start_y = 400;
#else
  const int start_y = 376;
#endif

  UI_TextSize(16);
  UI_TextColorEx(1, 1, 1, 0.5f);
  UI_DrawString(2, 2, "Hayden Kowalchuk / mrneo240 '20");

  UI_TextSize(20);

  /* Dont show Quit on console */
  for (int i = 0; i < MENU_ITEMS; i++) {
    /* Light Gray */
    UI_TextColor((236 / (float)255), (236 / (float)255), (236 / (float)255));
    float _alpha = (_fade_in ? (0.5f - (_next - now)) / 1.0f : ((_next - now)) / 1.0f);
    if (selected_index == i) {
      UI_TextColor((236 / (float)255) - _alpha, (236 / (float)255) - _alpha, (236 / (float)255) - _alpha);
    }

    UI_DrawString(start_x, start_y + (i * (spacing_h + text_size)), headers[i]);
  }
}
