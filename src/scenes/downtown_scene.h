/*
 * File: downtown_scene.h
 * Project: scenes
 * File Created: Tuesday, 2nd June 2020 12:19:06 am
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#pragma once
#include <scene/scene.h>

extern scene game_downtown;