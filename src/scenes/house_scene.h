/*
 * File: house_scene.h
 * Project: scenes
 * File Created: Monday, 4th May 2020 6:14:37 pm
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#pragma once

#include <scene/scene.h>
#include <stdbool.h>

extern scene game_house;
extern bool is_entering_house;
