#include "reply_scene.h"

#include <common/common.h>
#include <ui/ui_backend.h>

#include "../trigger/response.h"
#include "../trigger/neighbor.h"

static void Reply_Init(void);
static void Reply_Exit(void);
static void Reply_Update(float time);
static void Reply_Render2D(float time);

/* Registers the Reply scene and populates the struct */
//STARTUP_SCENE(&Reply_Init, &Reply_Exit, &Reply_Render2D, NULL, &Reply_Update, SCENE_BLOCK);
SCENE(reply_scene, &Reply_Init, &Reply_Exit, &Reply_Render2D, NULL, &Reply_Update, SCENE_BLOCK);

static void Reply_Init(void) {
}

static void Reply_Exit(void) {
}

static void Reply_Update(float time) {
  (void)time;
}

static void Reply_Render2D(float time) {
  (void)time;
  UI_DrawString(4, 4, neighbor_away->responses[0]);
  UI_DrawString(4, 20, neighbor_away->responses[1]);
  UI_DrawString(4, 36, neighbor_away->responses[2]);
}