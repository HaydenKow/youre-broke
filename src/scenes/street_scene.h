/*
 * File: street_scene.h
 * Project: scenes
 * File Created: Thursday, 7th May 2020 1:26:47 am
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#pragma once
#include <scene/scene.h>

extern scene game_street;