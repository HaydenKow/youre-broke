/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\placeholder\src\scenes\instructions_scene.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\placeholder\src\scenes
 * Created Date: Monday, May 4th 2020, 2:47:06 pm
 * Author: HaydenKow
 * 
 * Copyright (c) 2020 Hayden Kowalchuk
 */

#pragma once
#include <scene/scene.h>

extern scene scene_instructions;
