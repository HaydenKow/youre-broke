/*
 * File: car.c
 * Project: trigger
 * File Created: Sunday, 31st May 2020 11:08:00 pm
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#include <common/common.h>

#include "response.h"

static const response_array _house_enter = {
    .num = 2,
    .responses = {
        "This is your house do you want to go back inside?",
        "Home Sweet Home, enter?",
    },
};

static const response_array _house_leave = {
    .num = 3,
    .responses = {
        "Its scary out there is it time to go outside?",
        "You sure you want to head out there?",
        "Finally get bored of the house and need some air?",
    },
};

static const response_array _house_bed = {
    .num = 3,
    .responses = {
        "Your bed! This will be for recovering energy.",
        "Time to end the day and get some rest?",
        "Are you ready to tuck in for the evening?",
    },
};

static const response_array _house_kitchen = {
    .num = 3,
    .responses = {
        "A simple kitchen. You'll need to cook in the future.",
        "Time to raid the fridge and cook up a meal?",
        "Would you like to make some food for yourself?",
    },
};

const response_array *house_enter = &_house_enter;
const response_array *house_leave = &_house_leave;
const response_array *house_bed = &_house_bed;
const response_array *house_kitchen = &_house_kitchen;
