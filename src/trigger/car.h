/*
 * File: car.h
 * Project: trigger
 * File Created: Sunday, 31st May 2020 11:08:12 pm
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#pragma once

struct response_array;
typedef struct response_array response_array;

extern const response_array *car_unowned;
extern const response_array *car_owned;
extern const response_array *car_other;