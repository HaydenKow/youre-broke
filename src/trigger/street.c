/*
 * File: neighbor.c
 * Project: trigger
 * File Created: Thursday, 1st January 1970 12:00:00 am
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#include <common/common.h>

#include "response.h"

static const response_array _downtown_enter = {
    .num = 3,
    .responses = {
        "Time to go downtown?",
        "Fancy some shopping?",
        "Would you like to go to work?",
    },
};

static const response_array _downtown_exit = {
    .num = 2,
    .responses = {
        "Time to head home?",
        "Bored of being downtown?",
    },
};

const response_array *downtown_enter = &_downtown_enter;
const response_array *downtown_exit = &_downtown_exit;
