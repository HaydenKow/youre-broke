/*
 * File: car.c
 * Project: trigger
 * File Created: Sunday, 31st May 2020 11:08:00 pm
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#include <common/common.h>

#include "response.h"

static const response_array _car_unowned = {
    .num = 2,
    .responses = {
        "\"I wish I had a car\"",
        "One day you might be able to afford it",
    },
};

static const response_array _car_owned = {
    .num = 2,
    .responses = {
        "Do you want to get in your car?",
        "Planning on going somewhere?",
    },
};

static const response_array _car_other = {
    .num = 3,
    .responses = {
        "This isn't your car.",
        "Look but dont touch.",
        "What a sharp looking car.",
    },
};

const response_array *car_unowned = &_car_unowned;
const response_array *car_owned = &_car_owned;
const response_array *car_other = &_car_other;
