/*
 * File: neighbor.c
 * Project: trigger
 * File Created: Thursday, 1st January 1970 12:00:00 am
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#include <common/common.h>

#include "response.h"

static const response_array _neighbor_away = {
    .num = 3,
    .responses = {
        "Your neighbor lives here. They aren't home right now.",
        "Perhaps they aren't home at the moment.",
        "You start to knock but then you don't.",
    },
};

static const response_array _neighbor_empty = {
    .num = 3,
    .responses = {
        "No one lives here.",
        "The house is empty.",
        "They're away.",
    },
};

static const response_array _neighbor_enter = {
    .num = 3,
    .responses = {
        "No one lives here.",
        "The house is empty.",
        "They're away.",
    },
};

const response_array *neighbor_away = &_neighbor_away;
const response_array *neighbor_empty = &_neighbor_empty;
const response_array *neighbor_enter = &_neighbor_enter;
