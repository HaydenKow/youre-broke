
TARGET_EXEC ?= yourebroke
NAME = You Are Broke
SRC_DIRS = src

PSP_EBOOT_ICON ?= $(BUILD_DIR)/ICON0.PNG
PSP_EBOOT_UNKPNG ?= $(BUILD_DIR)/PIC1.PNG

include ../../makefiles/config.mak

.PHONY: test

test:
	cmd.exe /k "cd release\\windows && .\\$(TARGET_EXEC).exe && exit"